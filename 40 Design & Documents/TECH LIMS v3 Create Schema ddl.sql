
-- DROP TABLES IF EXISTS - Uncomment if needed
/*
TRUNCATE TABLE checkpoints CASCADE;

TRUNCATE TABLE order_comms CASCADE;

TRUNCATE TABLE order_comms_files CASCADE;

TRUNCATE TABLE lookup_items CASCADE;

TRUNCATE TABLE lookup_category CASCADE;

TRUNCATE TABLE test_types CASCADE;

TRUNCATE TABLE application_role_permissions CASCADE;

TRUNCATE TABLE application_permissions CASCADE;

TRUNCATE TABLE audit_details CASCADE;

TRUNCATE TABLE audits CASCADE;

TRUNCATE TABLE physical_article_data CASCADE;

TRUNCATE TABLE physical_articles CASCADE;

TRUNCATE TABLE hsmi_outputs CASCADE;

TRUNCATE TABLE hsmi_output_files CASCADE;

TRUNCATE TABLE application_zone_roles CASCADE;

TRUNCATE TABLE device_checks CASCADE;

TRUNCATE TABLE device_check_results CASCADE;

TRUNCATE TABLE application_notifications CASCADE;

TRUNCATE TABLE sample_test_device_measurements CASCADE;

TRUNCATE TABLE devices CASCADE;

TRUNCATE TABLE touch_points CASCADE;

TRUNCATE TABLE sample_test_stages CASCADE;

TRUNCATE TABLE test_steps CASCADE;

TRUNCATE TABLE application_user_roles CASCADE;

TRUNCATE TABLE application_roles CASCADE;

TRUNCATE TABLE bms_decisions CASCADE;

TRUNCATE TABLE sample_lab_test_data CASCADE;

TRUNCATE TABLE reagent_batch_data CASCADE;

TRUNCATE TABLE reagents CASCADE;

TRUNCATE TABLE sample_voids CASCADE;

TRUNCATE TABLE sample_comments CASCADE;

TRUNCATE TABLE sample_tracks CASCADE;

TRUNCATE TABLE settings CASCADE;

TRUNCATE TABLE setting_groups CASCADE;

TRUNCATE TABLE entity_attributes CASCADE;

TRUNCATE TABLE entities CASCADE;

TRUNCATE TABLE stored_items CASCADE;

TRUNCATE TABLE storage_box CASCADE;

TRUNCATE TABLE zones CASCADE;

TRUNCATE TABLE application_users CASCADE;

*/

CREATE TABLE IF NOT EXISTS checkpoints
(
    id         INTEGER NOT NULL,
    author_id  INTEGER,
    time_stamp TIMESTAMP WITH TIME ZONE,
    search     tsvector,
    data       jsonb   NOT NULL,
    CONSTRAINT pk_check_point
        PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS physical_articles
(
    id           INTEGER     NOT NULL,
    barcode      VARCHAR(50) NOT NULL,
    type_id      INTEGER     NOT NULL,
    capacity     INTEGER,
    state        INTEGER,
    is_empty     BOOLEAN,
    can_be_empty BOOLEAN,
    CONSTRAINT pk_physical_articles
        PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS hsmi_output_files
(
    id             INTEGER                  NOT NULL,
    filename       VARCHAR(50)              NOT NULL,
    date_created   TIMESTAMP WITH TIME ZONE NOT NULL,
    date_submitted TIMESTAMP WITH TIME ZONE,
    record_count   INTEGER,
    CONSTRAINT hsmi_output_file_pk
        PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS order_comms_files
(
    id            INTEGER                  NOT NULL,
    filename      VARCHAR(250)             NOT NULL,
    creation_date TIMESTAMP WITH TIME ZONE NOT NULL,
    import_date   TIMESTAMP WITH TIME ZONE,
    record_count  INTEGER,
    CONSTRAINT order_comms_file_pkey
        PRIMARY KEY (id),
    CONSTRAINT order_comms_file_filename_key
        UNIQUE (filename)
);

CREATE TABLE IF NOT EXISTS order_comms
(
    id                  INTEGER     NOT NULL,
    barcode_id          VARCHAR(50) NOT NULL,
    date_imported       TIMESTAMP WITH TIME ZONE,
    lab_barcode_id      VARCHAR(50),
    reference           VARCHAR(100),
    sample_taken        TIMESTAMP WITH TIME ZONE,
    sample_type         VARCHAR(50),
    test_request        VARCHAR(50),
    order_comms_file_id INTEGER     NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (barcode_id),
    CONSTRAINT order_comms_order_comms_file_id_fk
        FOREIGN KEY (order_comms_file_id) REFERENCES order_comms_files
);

CREATE TABLE IF NOT EXISTS lookup_category
(
    id                   INTEGER      NOT NULL,
    category_description VARCHAR(255) NOT NULL,
    start_date           DATE         NOT NULL,
    end_date             DATE,
    internal_use_only    SMALLINT,
    CONSTRAINT pk_lookup_category_category_id
        PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS lookup_items
(
    item_id           INTEGER      NOT NULL,
    category_id       INTEGER      NOT NULL,
    item_description  VARCHAR(255) NOT NULL,
    star_date         DATE         NOT NULL,
    end_date          DATE,
    lookup_code       VARCHAR(20)  NOT NULL,
    sequence_id       INTEGER,
    internal_use_only SMALLINT,
    CONSTRAINT pk_lookup_item_item_id
        PRIMARY KEY (item_id),
    CONSTRAINT lookup_items_lookup_category_id_fk
        FOREIGN KEY (category_id) REFERENCES lookup_category
);

CREATE TABLE IF NOT EXISTS test_types
(
    id          INTEGER     NOT NULL,
    name        VARCHAR(50) NOT NULL,
    description VARCHAR(500),
    CONSTRAINT pk_test_type
        PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS application_users
(
    id                     uuid         NOT NULL,
    user_name              VARCHAR(50)  NOT NULL,
    full_name              VARCHAR(150),
    email_address          VARCHAR(250) NOT NULL,
    phone_number           VARCHAR(50),
    password_hash          VARCHAR(100) NOT NULL,
    email_address_verified SMALLINT,
    is_locked              SMALLINT,
    created_on             TIMESTAMP(6),
    created_by             uuid,
    updated_on             TIMESTAMP(6),
    updated_by             uuid,
    CONSTRAINT pk_application_user_id
        PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS application_permissions
(
    id          INTEGER NOT NULL,
    name        VARCHAR(50),
    description VARCHAR(250),
    created_on  TIMESTAMP WITH TIME ZONE,
    created_by  uuid,
    updated_on  TIMESTAMP WITH TIME ZONE,
    updated_by  uuid,
    CONSTRAINT pk_application_permission_id
        PRIMARY KEY (id),
    CONSTRAINT application_permissions_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT application_permissions_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS application_roles
(
    id          INTEGER NOT NULL,
    name        VARCHAR(100),
    description VARCHAR(100),
    created_on  TIMESTAMP(6),
    created_by  uuid,
    updated_on  TIMESTAMP(6),
    updated_by  uuid,
    CONSTRAINT pk_application_role_id
        PRIMARY KEY (id),
    CONSTRAINT application_roles_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT application_roles_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS application_role_permissions
(
    role_id       INTEGER NOT NULL,
    permission_id INTEGER NOT NULL,
    CONSTRAINT application_role_permission_application_permission_id_fk
        FOREIGN KEY (permission_id) REFERENCES application_permissions,
    CONSTRAINT application_role_permission_application_role_id_fk
        FOREIGN KEY (role_id) REFERENCES application_roles
);

CREATE TABLE IF NOT EXISTS audits
(
    id           INTEGER                  NOT NULL,
    table_schema VARCHAR(50),
    table_name   VARCHAR(50)              NOT NULL,
    record_id    VARCHAR(50),
    record_key   VARCHAR(100),
    audit_action CHAR                     NOT NULL,
    modified_by  uuid                     NOT NULL,
    modified_on  TIMESTAMP WITH TIME ZONE NOT NULL,
    CONSTRAINT pk_audit_id
        PRIMARY KEY (id),
    CONSTRAINT audits_application_users_id_fk
        FOREIGN KEY (modified_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS audit_details
(
    id         INTEGER     NOT NULL,
    audit_id   INTEGER     NOT NULL,
    field_name VARCHAR(50) NOT NULL,
    new_value  TEXT,
    old_value  TEXT,
    CONSTRAINT pk_audit_details_id
        PRIMARY KEY (id),
    CONSTRAINT audit_details_audit_id_fk
        FOREIGN KEY (audit_id) REFERENCES audits
);

CREATE TABLE IF NOT EXISTS sample_tracks
(
    id                  INTEGER                  NOT NULL,
    barcode             VARCHAR(50)              NOT NULL,
    type_id             INTEGER                  NOT NULL,
    receipted_timestamp TIMESTAMP WITH TIME ZONE NOT NULL,
    result_id           INTEGER,
    status_id           INTEGER,
    tube_status_id      INTEGER,
    created_on          TIMESTAMP WITH TIME ZONE,
    created_by          uuid,
    updated_on          TIMESTAMP WITH TIME ZONE,
    updated_by          uuid,
    CONSTRAINT pk_sample_track
        PRIMARY KEY (id),
    CONSTRAINT sample_tracks_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT sample_tracks_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS physical_article_data
(
    id                  INTEGER NOT NULL,
    physical_article_id INTEGER NOT NULL,
    sample_track_id     INTEGER,
    scan_timstamp       TIMESTAMP WITH TIME ZONE,
    search              tsvector,
    data                jsonb   NOT NULL,
    CONSTRAINT pk_physical_article_data
        PRIMARY KEY (id),
    CONSTRAINT physical_article_data_physical_articles_id_fk
        FOREIGN KEY (physical_article_id) REFERENCES physical_articles,
    CONSTRAINT physical_article_data_sample_track_id_fk
        FOREIGN KEY (sample_track_id) REFERENCES sample_tracks
);

CREATE TABLE IF NOT EXISTS hsmi_outputs
(
    id                  INTEGER     NOT NULL,
    hsmi_output_file_id INTEGER     NOT NULL,
    barcode_id          VARCHAR(50) NOT NULL,
    lab_barcode_id      VARCHAR(50),
    reference           VARCHAR(150),
    test_lab            VARCHAR(100),
    sample_received     TIMESTAMP WITH TIME ZONE,
    sample_tested       TIMESTAMP WITH TIME ZONE,
    test_approved       TIMESTAMP WITH TIME ZONE,
    test_request        VARCHAR(100),
    result_name         VARCHAR(100),
    result_value        VARCHAR(50),
    result_comment      VARCHAR(50),
    sample_track_id     INTEGER     NOT NULL,
    CONSTRAINT hsmi_output_pk
        PRIMARY KEY (id),
    CONSTRAINT hsmi_output_barcode_id_key
        UNIQUE (barcode_id),
    CONSTRAINT hsmi_output_lab_barcode_id_key
        UNIQUE (lab_barcode_id),
    CONSTRAINT hsmi_output_hsmi_output_file_id_fk
        FOREIGN KEY (hsmi_output_file_id) REFERENCES hsmi_output_files,
    CONSTRAINT hsmi_output_sample_track_id_fk
        FOREIGN KEY (sample_track_id) REFERENCES sample_tracks
);

CREATE TABLE IF NOT EXISTS sample_lab_test_data
(
    id               INTEGER                  NOT NULL,
    test_type_id     INTEGER                  NOT NULL,
    sample_track_id  INTEGER                  NOT NULL,
    is_active        BOOLEAN,
    start_timestamp  TIMESTAMP WITH TIME ZONE NOT NULL,
    finish_timestamp TIMESTAMP WITH TIME ZONE,
    data             jsonb                    NOT NULL,
    search           tsvector,
    CONSTRAINT pk_test_data
        PRIMARY KEY (id),
    CONSTRAINT test_data_sample_tracks_id_fk
        FOREIGN KEY (sample_track_id) REFERENCES sample_tracks
);

CREATE TABLE IF NOT EXISTS sample_comments
(
    id              INTEGER                  NOT NULL,
    author_id       INTEGER,
    sample_track_id INTEGER                  NOT NULL,
    comment         VARCHAR(2000)            NOT NULL,
    category_id     INTEGER,
    created_date    TIMESTAMP WITH TIME ZONE NOT NULL,
    created_by      uuid,
    updated_date    TIMESTAMP WITH TIME ZONE,
    updated_by      uuid,
    CONSTRAINT pk_sample_comment
        PRIMARY KEY (id),
    CONSTRAINT sample_comment_sample_track_id_fk
        FOREIGN KEY (sample_track_id) REFERENCES sample_tracks,
    CONSTRAINT sample_comments_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT sample_comments_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS setting_groups
(
    id          INTEGER     NOT NULL,
    code        VARCHAR(50) NOT NULL,
    description VARCHAR(250),
    is_active   BOOLEAN,
    created_on  TIMESTAMP WITH TIME ZONE,
    created_by  uuid,
    updated_on  TIMESTAMP WITH TIME ZONE,
    updated_by  uuid,
    CONSTRAINT setting_groups_pk
        PRIMARY KEY (id),
    CONSTRAINT setting_groups_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT setting_groups_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS zones
(
    id          INTEGER     NOT NULL,
    code        VARCHAR(10) NOT NULL,
    description VARCHAR(250),
    is_active   BOOLEAN,
    sequence    INTEGER,
    created_on  TIMESTAMP WITH TIME ZONE,
    created_by  uuid,
    updated_on  TIMESTAMP WITH TIME ZONE,
    updated_by  uuid,
    CONSTRAINT pk_zone_id
        PRIMARY KEY (id),
    CONSTRAINT zones_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT zones_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS application_zone_roles
(
    role_id INTEGER NOT NULL,
    zone_id INTEGER NOT NULL,
    CONSTRAINT application_zone_role_application_role_id_fk
        FOREIGN KEY (role_id) REFERENCES application_roles,
    CONSTRAINT application_zone_role_zone_id_fk
        FOREIGN KEY (role_id) REFERENCES zones
);

CREATE TABLE IF NOT EXISTS devices
(
    id         INTEGER      NOT NULL,
    name       VARCHAR(250) NOT NULL,
    ip_address VARCHAR(50),
    url        VARCHAR(250),
    status     INTEGER,
    type_id    INTEGER      NOT NULL,
    zone_id    INTEGER,
    created_on TIMESTAMP(6),
    created_by uuid,
    updated_on TIMESTAMP WITH TIME ZONE,
    updated_by uuid,
    CONSTRAINT pk_device_id
        PRIMARY KEY (id),
    CONSTRAINT device_zone_id_fk
        FOREIGN KEY (id) REFERENCES zones,
    CONSTRAINT devices_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT devices_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS device_checks
(
    id                 INTEGER NOT NULL,
    device_id          INTEGER NOT NULL,
    check_type_id      INTEGER,
    expected_min_value INTEGER,
    expected_max_value INTEGER,
    measurement_unit   VARCHAR(50),
    frequency_id       INTEGER,
    is_active          BOOLEAN,
    created_on         TIMESTAMP WITH TIME ZONE,
    created_by         uuid,
    updated_by         uuid,
    updated_on         TIMESTAMP WITH TIME ZONE,
    CONSTRAINT pk_device_check_id
        PRIMARY KEY (id),
    CONSTRAINT device_check_device_id_fk
        FOREIGN KEY (device_id) REFERENCES devices,
    CONSTRAINT device_checks_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT device_checks_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS device_check_results
(
    id              INTEGER                  NOT NULL,
    device_id       INTEGER,
    check_timestamp TIMESTAMP WITH TIME ZONE NOT NULL,
    check_type_id   INTEGER,
    value           INTEGER,
    created_on      TIMESTAMP WITH TIME ZONE,
    created_by      uuid,
    updated_on      TIMESTAMP WITH TIME ZONE,
    updated_by      uuid,
    CONSTRAINT pk_device_check_result_id
        PRIMARY KEY (id),
    CONSTRAINT device_check_result_device_id_fk
        FOREIGN KEY (device_id) REFERENCES devices,
    CONSTRAINT device_check_results_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT device_check_results_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS application_notifications
(
    id                         INTEGER      NOT NULL,
    from_zone_id               INTEGER,
    to_zone_id                 INTEGER,
    recipients                 VARCHAR(250) NOT NULL,
    subject                    VARCHAR(250) NOT NULL,
    body                       TEXT         NOT NULL,
    template_id                INTEGER,
    notification_delivery_type INTEGER,
    url                        VARCHAR(250),
    status                     SMALLINT,
    created_on                 TIMESTAMP WITH TIME ZONE,
    created_by                 uuid,
    updated_on                 TIMESTAMP WITH TIME ZONE,
    updated_by                 uuid,
    CONSTRAINT pk_application_notification_id
        PRIMARY KEY (id),
    CONSTRAINT application_notification_zone_id_fk
        FOREIGN KEY (from_zone_id) REFERENCES zones,
    CONSTRAINT application_notification_zone_id_fk_2
        FOREIGN KEY (to_zone_id) REFERENCES zones,
    CONSTRAINT application_notifications_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT application_notifications_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS sample_test_device_measurements
(
    id              INTEGER                  NOT NULL,
    sample_track_id INTEGER                  NOT NULL,
    device_id       INTEGER                  NOT NULL,
    time_stamp      TIMESTAMP WITH TIME ZONE NOT NULL,
    data            jsonb                    NOT NULL,
    search          tsvector,
    CONSTRAINT pk_sample_test_device_measurement
        PRIMARY KEY (id),
    CONSTRAINT sample_test_device_measurement_devices_id_fk
        FOREIGN KEY (device_id) REFERENCES devices,
    CONSTRAINT sample_test_device_measurement_sample_track_id_fk
        FOREIGN KEY (sample_track_id) REFERENCES sample_tracks
);

CREATE TABLE IF NOT EXISTS touch_points
(
    id               INTEGER     NOT NULL,
    code             VARCHAR(50) NOT NULL,
    description      VARCHAR(250),
    zone_id          INTEGER,
    point_identifier INTEGER,
    point_type_id    INTEGER,
    sequence         INTEGER,
    is_active        BOOLEAN,
    created_on       TIMESTAMP WITH TIME ZONE,
    created_by       uuid,
    updated_on       TIMESTAMP WITH TIME ZONE,
    updated_by       uuid,
    CONSTRAINT pk_touch_point_id
        PRIMARY KEY (id),
    CONSTRAINT touch_points_zone_id_fk
        FOREIGN KEY (zone_id) REFERENCES zones,
    CONSTRAINT touch_points_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT touch_points_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS test_steps
(
    id           INTEGER                  NOT NULL,
    test_type_id INTEGER                  NOT NULL,
    name         VARCHAR(50)              NOT NULL,
    code         VARCHAR(20),
    description  VARCHAR(250),
    sequence     INTEGER                  NOT NULL,
    start_date   TIMESTAMP WITH TIME ZONE NOT NULL,
    end_date     TIMESTAMP WITH TIME ZONE,
    zone_id      INTEGER,
    CONSTRAINT pk_test_steps
        PRIMARY KEY (id),
    CONSTRAINT test_steps_zone_id_fk
        FOREIGN KEY (zone_id) REFERENCES zones
);

CREATE TABLE IF NOT EXISTS sample_test_stages
(
    id               INTEGER NOT NULL,
    sample_track_id  INTEGER NOT NULL,
    current_step_id  INTEGER,
    previous_step_id INTEGER,
    created_on       TIMESTAMP WITH TIME ZONE,
    created_by       uuid,
    updated_on       TIMESTAMP WITH TIME ZONE,
    updated_by       uuid,
    CONSTRAINT pk_sample_test_stages
        PRIMARY KEY (id),
    CONSTRAINT sample_test_stages_sample_track_id_fk
        FOREIGN KEY (sample_track_id) REFERENCES sample_tracks,
    CONSTRAINT sample_test_stages_test_steps_id_fk
        FOREIGN KEY (current_step_id) REFERENCES test_steps,
    CONSTRAINT sample_test_stages_test_steps_id_fk_2
        FOREIGN KEY (previous_step_id) REFERENCES test_steps,
    CONSTRAINT sample_test_stages_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT sample_test_stages_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS application_user_roles
(
    user_id uuid    NOT NULL,
    role_id INTEGER NOT NULL,
    CONSTRAINT application_user_role_application_user_id_fk
        FOREIGN KEY (user_id) REFERENCES application_users,
    CONSTRAINT application_user_role_application_role_id_fk
        FOREIGN KEY (role_id) REFERENCES application_roles
);

CREATE TABLE IF NOT EXISTS bms_decisions
(
    id                 INTEGER                  NOT NULL,
    decision_timestamp TIMESTAMP WITH TIME ZONE NOT NULL,
    result_id          INTEGER                  NOT NULL,
    test_id            INTEGER                  NOT NULL,
    user_id            uuid                     NOT NULL,
    is_final           BIT                      NOT NULL,
    created_date       TIMESTAMP WITH TIME ZONE,
    created_by         uuid,
    updated_date       TIMESTAMP WITH TIME ZONE,
    updated_by         uuid,
    CONSTRAINT pk_bms_decision
        PRIMARY KEY (id),
    CONSTRAINT bms_decision_application_user_id_fk
        FOREIGN KEY (user_id) REFERENCES application_users,
    CONSTRAINT bms_decisions_sample_lab_test_data_id_fk
        FOREIGN KEY (test_id) REFERENCES sample_lab_test_data,
    CONSTRAINT bms_decisions_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT bms_decisions_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS sample_voids
(
    id                INTEGER                  NOT NULL,
    void_timestamp    TIMESTAMP WITH TIME ZONE NOT NULL,
    sample_track_id   INTEGER,
    user_id           VARCHAR(64),
    reason_type_id    INTEGER,
    sample_comment_id INTEGER,
    created_date      TIMESTAMP WITH TIME ZONE,
    created_by        uuid,
    updated_date      TIMESTAMP WITH TIME ZONE,
    updated_by        uuid,
    CONSTRAINT pk_sample_voids_id
        PRIMARY KEY (id),
    CONSTRAINT fk_sample_voids_sample_tracks_id
        FOREIGN KEY (sample_track_id) REFERENCES sample_tracks,
    CONSTRAINT fk_sample_voids_sample_comments_id
        FOREIGN KEY (sample_comment_id) REFERENCES sample_comments,
    CONSTRAINT sample_voids_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT sample_voids_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS settings
(
    id          INTEGER     NOT NULL,
    group_id    INTEGER,
    code        VARCHAR(50) NOT NULL,
    description VARCHAR(250),
    data_type   VARCHAR(50),
    value       VARCHAR(10) NOT NULL,
    is_active   BOOLEAN,
    created_on  TIMESTAMP WITH TIME ZONE,
    created_by  uuid,
    updated_on  TIMESTAMP WITH TIME ZONE,
    updated_by  uuid,
    CONSTRAINT settings_setting_groups_id_fk
        FOREIGN KEY (group_id) REFERENCES setting_groups,
    CONSTRAINT settings_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT settings_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS entities
(
    id              INTEGER     NOT NULL,
    name            VARCHAR(50) NOT NULL,
    base_data       TEXT,
    schema_field_id INTEGER     NOT NULL,
    CONSTRAINT pk_entities
        PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS entity_attributes
(
    id        INTEGER      NOT NULL,
    entity_id INTEGER      NOT NULL,
    name      VARCHAR(150) NOT NULL,
    data_type VARCHAR(50)  NOT NULL,
    CONSTRAINT pk_entity_attributes
        PRIMARY KEY (id),
    CONSTRAINT entity_attributes_entities_id_fk
        FOREIGN KEY (entity_id) REFERENCES entities
);

CREATE TABLE IF NOT EXISTS stored_items
(
    id             INTEGER                  NOT NULL,
    item_id        INTEGER                  NOT NULL,
    item_type_id   INTEGER,
    storage_box_id INTEGER                  NOT NULL,
    in_timestamp   TIMESTAMP WITH TIME ZONE NOT NULL,
    out_timestamp  TIMESTAMP WITH TIME ZONE,
    created_date   TIMESTAMP WITH TIME ZONE,
    created_by     uuid,
    updated_date   TIMESTAMP WITH TIME ZONE,
    updated_by     uuid,
    CONSTRAINT pk_stored_item_id
        PRIMARY KEY (id),
    CONSTRAINT stored_items_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT stored_items_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS storage_box
(
    id             INTEGER     NOT NULL,
    barcode_id     VARCHAR(50) NOT NULL,
    state          INTEGER     NOT NULL,
    is_empty       BOOLEAN     NOT NULL,
    is_full        BOOLEAN     NOT NULL,
    can_be_emptied BOOLEAN     NOT NULL,
    capacity       INTEGER     NOT NULL,
    zone_id        INTEGER,
    created_date   TIMESTAMP WITH TIME ZONE,
    created_by     uuid,
    updated_date   TIMESTAMP WITH TIME ZONE,
    updated_by     uuid,
    CONSTRAINT pk_storage_box_id
        PRIMARY KEY (id),
    CONSTRAINT storage_box_zone_id_fk
        FOREIGN KEY (zone_id) REFERENCES zones,
    CONSTRAINT storage_box_application_users_id_fk
        FOREIGN KEY (created_by) REFERENCES application_users,
    CONSTRAINT storage_box_application_users_id_fk_2
        FOREIGN KEY (updated_by) REFERENCES application_users
);

CREATE TABLE IF NOT EXISTS reagents
(
    id              INTEGER     NOT NULL,
    lot_number      VARCHAR(50) NOT NULL,
    batch_code    VARCHAR(50) NOT NULL,
    name            VARCHAR(50) NOT NULL,
    description     VARCHAR(50),
    type_id         INTEGER     NOT NULL,
    approval_date   TIMESTAMP WITH TIME ZONE,
    approved_by     uuid,
    thaw_date     TIMESTAMP WITH TIME ZONE,
    use_by_date     TIMESTAMP WITH TIME ZONE,
    expiry_date     TIMESTAMP WITH TIME ZONE,
    CONSTRAINT pk_reagents
        PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS reagent_batch_data
(
    id                   INTEGER NOT NULL,
    reagent_id           INTEGER NOT NULL,
    author_id            INTEGER,
    sample_track_id      INTEGER,
    qc_author_id         VARCHAR(50),
    qc_result            VARCHAR(50),
    qc_result_time_stamp TIMESTAMP WITH TIME ZONE,
    data                 jsonb   NOT NULL,
    CONSTRAINT fk_reagent_data_reagents
        PRIMARY KEY (id),

    CONSTRAINT reagent_batch_data_reagents_id_fk
        FOREIGN KEY (reagent_id) REFERENCES reagents,
        CONSTRAINT reagent_batch_data_sample_track_id_fk
        FOREIGN KEY (sample_track_id) REFERENCES sample_tracks
);

