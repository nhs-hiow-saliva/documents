CREATE TABLE `application_permission` (
  `id` int NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `application_role` (
  `id` int NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `application_role_permission` (
  `role_id` int NOT NULL,
  `permission_id` int NOT NULL,
  KEY `FK_application_role_permission_application_role` (`role_id`),
  KEY `FK_application_role_permission_application_permission` (`permission_id`),
  CONSTRAINT `FK_application_role_permission_application_permission` FOREIGN KEY (`permission_id`) REFERENCES `application_permission` (`id`),
  CONSTRAINT `FK_application_role_permission_application_role` FOREIGN KEY (`role_id`) REFERENCES `application_role` (`id`)
);

CREATE TABLE `application_user` (
  `id` int NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `full_name` varchar(150) DEFAULT NULL,
  `email_address` varchar(250) NOT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `password_hash` varchar(100) NOT NULL,
  `email_address_verified` tinyint(1) DEFAULT NULL,
  `is_locked` tinyint(1) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `application_user_role` (
  `user_id` int NOT NULL,
  `role_id` int NOT NULL,
  KEY `FK_application_user_role_application_user` (`user_id`),
  KEY `FK_application_user_role_application_role` (`role_id`),
  CONSTRAINT `FK_application_user_role_application_role` FOREIGN KEY (`role_id`) REFERENCES `application_role` (`id`),
  CONSTRAINT `FK_application_user_role_application_user` FOREIGN KEY (`user_id`) REFERENCES `application_user` (`id`)
);

CREATE TABLE `application_zone_role` (
  `role_id` int NOT NULL,
  `zone_id` int NOT NULL,
  KEY `FK_application_zone_role_application_role` (`role_id`),
  KEY `FK_application_zone_role_zone` (`zone_id`),
  CONSTRAINT `FK_application_zone_role_application_role` FOREIGN KEY (`role_id`) REFERENCES `application_role` (`id`),
  CONSTRAINT `FK_application_zone_role_zone` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`)
);

CREATE TABLE `audit` (
  `id` int NOT NULL,
  `table_schema` varchar(50) DEFAULT NULL,
  `table_name` varchar(50) NOT NULL,
  `record_id` varchar(50) DEFAULT NULL,
  `record_key` varchar(100) DEFAULT NULL,
  `audit_action` char(1) NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_on` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `audit_details` (
  `id` int NOT NULL,
  `audit_id` int NOT NULL,
  `field_name` varchar(50) NOT NULL,
  `new_value` longtext,
  `old_value` longtext,
  PRIMARY KEY (`id`),
  KEY `FK_audit_details_audit` (`audit_id`),
  CONSTRAINT `FK_audit_details_audit` FOREIGN KEY (`audit_id`) REFERENCES `audit` (`id`)
);

CREATE TABLE `bms_decision` (
  `id` int NOT NULL,
  `sample_track_id` int NOT NULL,
  `decision_timestamp` datetime(6) DEFAULT NULL,
  `user_id` varchar(64) DEFAULT NULL,
  `result` varchar(1) DEFAULT NULL,
  `comment_id` int DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bms_decision_sample_track` (`sample_track_id`),
  CONSTRAINT `FK_bms_decision_sample_track` FOREIGN KEY (`sample_track_id`) REFERENCES `sample_track` (`id`)
);

CREATE TABLE `device` (
  `id` int NOT NULL,
  `name` varchar(250) NOT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `type_id` int NOT NULL,
  `zone_id` int DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_device_zone` (`zone_id`),
  CONSTRAINT `FK_device_zone` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`)
);

CREATE TABLE `device_check` (
  `id` int NOT NULL,
  `device_id` int NOT NULL,
  `check_type_id` int DEFAULT NULL,
  `expected_min_value` int DEFAULT NULL,
  `expected_max_value` int DEFAULT NULL,
  `measurement_unit` varchar(50) DEFAULT NULL,
  `frequency_id` int DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `device_check_device_id_fk` (`device_id`),
  CONSTRAINT `device_check_device_id_fk` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
);

CREATE TABLE `device_check_result` (
  `id` int NOT NULL,
  `device_id` int DEFAULT NULL,
  `check_timestamp` datetime(6) NOT NULL,
  `check_type_id` int DEFAULT NULL,
  `value` int DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `device_check_result_device_id_fk` (`device_id`),
  CONSTRAINT `device_check_result_device_id_fk` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
);

CREATE TABLE `genie_block` (
  `id` int NOT NULL,
  `barcode` varchar(50) DEFAULT NULL,
  `genie_block_rack_id` varchar(50) DEFAULT NULL,
  `genie_block_rack_index` int DEFAULT NULL,
  `mastermix_batch_id` int DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `assembly_time` datetime(6) DEFAULT NULL,
  `mastermix_dispense_time` datetime(6) DEFAULT NULL,
  `template_addition_time` datetime(6) DEFAULT NULL,
  `genie_load_time` datetime(6) DEFAULT NULL,
  `recycled_time` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `hsmi_output` (
  `id` int NOT NULL,
  `barcode` varchar(50) DEFAULT NULL,
  `lab_barcode` varchar(50) DEFAULT NULL,
  `reference` varchar(1000) DEFAULT NULL,
  `test_lab` varchar(250) DEFAULT NULL,
  `hsmi_output_file_id` int DEFAULT NULL,
  `sample_received_on` datetime(6) DEFAULT NULL,
  `sample_tested_on` datetime(6) DEFAULT NULL,
  `test_requested_on` datetime(6) DEFAULT NULL,
  `test_approved_on` datetime(6) DEFAULT NULL,
  `result_name` varchar(100) DEFAULT NULL,
  `result_value` varchar(100) DEFAULT NULL,
  `sample_id` int DEFAULT NULL,
  `comment` longtext,
  PRIMARY KEY (`id`),
  KEY `FK_hsmi_output_hsmi_output_file` (`hsmi_output_file_id`),
  KEY `FK_hsmi_output_sample` (`sample_id`),
  CONSTRAINT `FK_hsmi_output_hsmi_output_file` FOREIGN KEY (`hsmi_output_file_id`) REFERENCES `hsmi_output_file` (`id`),
  CONSTRAINT `FK_hsmi_output_sample` FOREIGN KEY (`sample_id`) REFERENCES `sample` (`id`)
);

CREATE TABLE `hsmi_output_file` (
  `id` int NOT NULL,
  `filename` varchar(250) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `submitted_on` datetime(6) DEFAULT NULL,
  `record_count` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `lab_data` (
  `id` int NOT NULL,
  `activity_timestamp` datetime(6) NOT NULL,
  `sample_track_id` int DEFAULT NULL,
  `device_id` varchar(250) DEFAULT NULL,
  `physical_article_id` varchar(250) DEFAULT NULL,
  `identifier_id` varchar(250) DEFAULT NULL,
  `identifier_type_id` varchar(250) DEFAULT NULL,
  `measurement_type_id` varchar(250) DEFAULT NULL,
  `measurement_index` int DEFAULT NULL,
  `measurement_value` varchar(250) DEFAULT NULL,
  `invalid_reason_id` int DEFAULT NULL,
  `sample_comment_id` int DEFAULT NULL,
  `lab_data_json` longtext,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sample_track_lab_data_sample_track` (`sample_track_id`),
  CONSTRAINT `FK_sample_track_lab_data_sample_track` FOREIGN KEY (`sample_track_id`) REFERENCES `sample_track` (`id`)
);

CREATE TABLE `lamp_test` (
  `id` int NOT NULL,
  `sample_track_id` int NOT NULL,
  `test_type_id` int DEFAULT NULL,
  `mastermix_batch_id` varchar(100) DEFAULT NULL,
  `lysis_buffer_batch_id` varchar(100) DEFAULT NULL,
  `receipt_timestamp` datetime(6) DEFAULT NULL,
  `receipted_by` varchar(100) DEFAULT NULL,
  `aliquotted_timestamp` datetime(6) DEFAULT NULL,
  `aliquotted_by` varchar(100) DEFAULT NULL,
  `aliquotted_well_plate_id` varchar(100) DEFAULT NULL,
  `aliquoted_well_plate_index` int DEFAULT NULL,
  `template_added_timestamp` datetime(6) DEFAULT NULL,
  `template_added_by` varchar(100) DEFAULT NULL,
  `genie_rack_id` varchar(100) DEFAULT NULL,
  `genie_block_id` varchar(100) DEFAULT NULL,
  `extraction_timestamp` datetime(6) DEFAULT NULL,
  `extraction_by` varchar(100) DEFAULT NULL,
  `extraction_plate_id` varchar(100) DEFAULT NULL,
  `pcr_plate_id` varchar(100) DEFAULT NULL,
  `loaded_timestamp` datetime(6) DEFAULT NULL,
  `loaded_by` varchar(100) DEFAULT NULL,
  `run_finished_timestamp` datetime(6) DEFAULT NULL,
  `run_finished_by` varchar(100) DEFAULT NULL,
  `result` varchar(50) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `lookup_category` (
  `category_id` int NOT NULL,
  `category_description` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `internal_use_only` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
);

CREATE TABLE `lookup_item` (
  `item_id` int NOT NULL,
  `category_id` int NOT NULL,
  `item_description` varchar(255) NOT NULL,
  `star_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `lookup_code` varchar(20) NOT NULL,
  `sequence_id` int DEFAULT NULL,
  `internal_use_only` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `FK_lookup_item_lookup_category` (`category_id`),
  CONSTRAINT `FK_lookup_item_lookup_category` FOREIGN KEY (`category_id`) REFERENCES `lookup_category` (`category_id`)
);

CREATE TABLE `notification` (
  `id` int NOT NULL,
  `from_zone_id` int DEFAULT NULL,
  `to_zone_id` int DEFAULT NULL,
  `recipients` longtext,
  `subject` varchar(250) DEFAULT NULL,
  `body` longtext,
  `template_id` int DEFAULT NULL,
  `notification_delivery_type` int DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `order_comms` (
  `id` int NOT NULL,
  `barcode` varchar(50) DEFAULT NULL,
  `lab_barcode` varchar(50) DEFAULT NULL,
  `imported_on` datetime(6) DEFAULT NULL,
  `reference` varchar(100) DEFAULT NULL,
  `sample_taken_on` datetime(6) DEFAULT NULL,
  `sample_type_id` int DEFAULT NULL,
  `test_request` varchar(500) DEFAULT NULL,
  `order_comms_file_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_order_comms_order_comms_file` (`order_comms_file_id`),
  KEY `FK_order_comms_sample` (`barcode`),
  CONSTRAINT `FK_order_comms_order_comms_file` FOREIGN KEY (`order_comms_file_id`) REFERENCES `order_comms_file` (`id`),
  CONSTRAINT `FK_order_comms_sample` FOREIGN KEY (`barcode`) REFERENCES `sample` (`barcode`)
);

CREATE TABLE `order_comms_file` (
  `id` int NOT NULL,
  `filename` varchar(250) DEFAULT NULL,
  `imported_on` datetime(6) DEFAULT NULL,
  `record_count` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `physical_article` (
  `id` int NOT NULL,
  `barcode` varchar(50) NOT NULL,
  `physical_article_type_id` int NOT NULL,
  `capacity` int DEFAULT NULL,
  `state` int DEFAULT NULL,
  `is_empty` tinyint(1) DEFAULT NULL,
  `can_be_emptied` tinyint(1) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `reagent` (
  `id` int NOT NULL,
  `name` varchar(200) NOT NULL,
  `barcode` varchar(50) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `reagent_type_id` int NOT NULL,
  `first_used_date` datetime(6) DEFAULT NULL,
  `use_by_date` datetime(6) DEFAULT NULL,
  `expiry_date` datetime(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `sample` (
  `id` int NOT NULL,
  `barcode` varchar(50) NOT NULL,
  `sample_type_id` int DEFAULT NULL,
  `timestamp_scanned` datetime(6) NOT NULL,
  `result_type_id` int DEFAULT NULL,
  `test_status` int DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IX_sample_barcode` (`barcode`)
);

CREATE TABLE `sample_comment` (
  `id` int NOT NULL,
  `comment_timestamp` datetime(6) NOT NULL,
  `comment` longtext,
  `userid` varchar(64) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `sample_track_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `sample_track` (
  `id` int NOT NULL,
  `sample_id` int NOT NULL,
  `test_uid` varchar(64) NOT NULL,
  `receipt_timestamp` datetime(6) NOT NULL,
  `discard_tube` tinyint(1) DEFAULT NULL,
  `retest_tube` tinyint(1) DEFAULT NULL,
  `test_status` int DEFAULT NULL,
  `test_type` int DEFAULT NULL,
  `sample_comment_id` int DEFAULT NULL,
  `is_active_test` tinyint(1) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sample_track_sample` (`sample_id`),
  KEY `FK_sample_track_sample_comment` (`sample_comment_id`),
  CONSTRAINT `FK_sample_track_sample` FOREIGN KEY (`sample_id`) REFERENCES `sample` (`id`),
  CONSTRAINT `FK_sample_track_sample_comment` FOREIGN KEY (`sample_comment_id`) REFERENCES `sample_comment` (`id`)
);

CREATE TABLE `storage_box` (
  `id` int NOT NULL,
  `state` varchar(10) NOT NULL,
  `is_empty` tinyint(1) NOT NULL,
  `is_full` tinyint(1) NOT NULL,
  `can_be_emptied` tinyint(1) NOT NULL,
  `capacity` int NOT NULL,
  `barcode_id` varchar(50) NOT NULL,
  `zone_id` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `stored_item` (
  `id` int NOT NULL,
  `author_id` varchar(50) NOT NULL,
  `storage_box_id` varchar(50) NOT NULL,
  `item_barcode` varchar(50) NOT NULL,
  `item_type` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `touch_point` (
  `id` int NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `zone_id` int DEFAULT NULL,
  `point_identifier` int DEFAULT NULL,
  `point_type_id` int DEFAULT NULL,
  `sequence` int DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_touch_point_zone` (`zone_id`),
  CONSTRAINT `FK_touch_point_zone` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`)
);

CREATE TABLE `void_sample` (
  `id` int NOT NULL,
  `void_timestamp` datetime(6) NOT NULL,
  `sample_id` int DEFAULT NULL,
  `user_id` varchar(64) DEFAULT NULL,
  `reason_type_id` int DEFAULT NULL,
  `sample_comment_id` int DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_void_sample_sample` (`sample_id`),
  KEY `FK_void_sample_sample_comment` (`sample_comment_id`),
  CONSTRAINT `FK_void_sample_sample` FOREIGN KEY (`sample_id`) REFERENCES `sample` (`id`),
  CONSTRAINT `FK_void_sample_sample_comment` FOREIGN KEY (`sample_comment_id`) REFERENCES `sample_comment` (`id`)
);

CREATE TABLE `well_plate` (
  `id` int NOT NULL,
  `barcode` varchar(50) NOT NULL,
  `lysis_buffer_batch_id` int DEFAULT NULL,
  `aliquoted_author_id` int DEFAULT NULL,
  `well_plate_prep_timestamp` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `zone` (
  `id` int NOT NULL,
  `code` varchar(10) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `sequence` int DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_on` datetime(6) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_zone_storage_box` FOREIGN KEY (`id`) REFERENCES `storage_box` (`id`)
);
